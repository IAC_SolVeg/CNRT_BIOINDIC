###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: export data from QIIME2 artifact.qza and visualization.qzv files
# Date: 2017.11.02
# Run: snakemake -j [core] --use-conda
# Latest modification: 2018.11.20
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]
PRIMERS = config["datasets"]["primers"]

# Environment
QIIME2 = "../env/qiime2-2018.08.yaml"

PCOA, = glob_wildcards("out/"+PROJECT+"/"+PRIMERS[0]+"/qiime2/pcoa/"
                       +"{pcoa}.qza")

TREE, = glob_wildcards("out/"+PROJECT+"/"+PRIMERS[0]+"/qiime2/tree/"
                       +"{tree}.qza")

TAXO, = glob_wildcards("out/"+PROJECT+"/"+PRIMERS[0]+"/qiime2/taxonomy/"
                       +"{taxo}.qza")

VISU, = glob_wildcards("out/"+PROJECT+"/"+PRIMERS[0]+"/qiime2/visual/"
                       +"{visu}.qzv")

CORE, = glob_wildcards("out/"+PROJECT+"/"+PRIMERS[0]+"/qiime2/core/"
                       +"{core}.qza")

SUBT, = glob_wildcards("out/"+PROJECT+"/"+PRIMERS[0]+"/qiime2/subtables/"
                       +"{subt}.qza")

###############################################################################
rule all:
    input:    
        pcoa = expand("out/{project}/{primers}/qiime2/export/PCoA/{pcoa}/",
                      project = PROJECT,
                      primers = PRIMERS,
                      pcoa = PCOA),

        tree = expand("out/{project}/{primers}/qiime2/export/tree/{tree}/",
                      project = PROJECT,
                      primers = PRIMERS,
                      tree = TREE),

        taxo = expand("out/{project}/{primers}/qiime2/export/taxonomy/{taxo}/",
                      project = PROJECT,
                      primers = PRIMERS,
                      taxo = TAXO),

        visu = expand("out/{project}/{primers}/qiime2/export/visual/{visu}/",
                      project = PROJECT,
                      primers = PRIMERS,
                      visu = VISU),
        
        core = expand("out/{project}/{primers}/qiime2/export/core/{core}/",
                      project = PROJECT,
                      primers = PRIMERS,
                      core = CORE),
        
        subt = expand("out/{project}/{primers}/qiime2/export/subtables/{subt}/",
                      project = PROJECT,
                      primers = PRIMERS,
                      subt = SUBT)

###############################################################################
rule export_pcoa:
    # Aim: Export QIIME2 pcoa artifact.qza
    # Use: qiime tools export [OPTIONS] PATH
    conda:
        QIIME2
    input:
        Artifact = "out/{project}/{primers}/qiime2/pcoa/{pcoa}.qza"
    output:
        Export = directory("out/{project}/{primers}/qiime2/export/PCoA/"
                           +"{pcoa}/")
    shell:
        "qiime tools export "
        "--input-path {input.Artifact} "
        "--output-path {output.Export}"

###############################################################################
rule export_tree:
    # Aim: Export QIIME2 tree artifact.qza
    # Use: qiime tools export [OPTIONS] PATH
    conda:
        QIIME2
    input:
        Artifact = "out/{project}/{primers}/qiime2/tree/{tree}.qza"
    output:
        Export = directory("out/{project}/{primers}/qiime2/export/tree/"
                           +"{tree}/")
    shell:
        "qiime tools export "
        "--input-path {input.Artifact} "
        "--output-path {output.Export}"

###############################################################################
rule export_taxonomy:
    # Aim: Export QIIME2 taxonomy artifact.qza
    # Use: qiime tools export [OPTIONS] PATH
    conda:
        QIIME2
    input:
        Artifact = "out/{project}/{primers}/qiime2/taxonomy/{taxo}.qza"
    output:
        Export = directory("out/{project}/{primers}/qiime2/export/taxonomy/"
                           +"{taxo}/")
    shell:
        "qiime tools export "
        "--input-path {input.Artifact} "
        "--output-path {output.Export}"

###############################################################################
rule export_visualisation:
    # Aim: Export QIIME2 visual visualization.qzv
    # Use: qiime tools export [OPTIONS] PATH
    conda:
        QIIME2
    input:
        Artifact = "out/{project}/{primers}/qiime2/visual/{visu}.qzv"
    output:
        Export = directory("out/{project}/{primers}/qiime2/export/visual/"
                           +"{visu}/")
    shell:
        "qiime tools export "
        "--input-path {input.Artifact} "
        "--output-path {output.Export}"

###############################################################################
rule export_core:
    # Aim: Export QIIME2 core artifact.qza
    # Use: qiime tools export [OPTIONS] PATH
    conda:
        QIIME2
    input:
        Artifact = "out/{project}/{primers}/qiime2/core/{core}.qza"
    output:
        Export = directory("out/{project}/{primers}/qiime2/export/core/"
                           +"{core}/")
    shell:
        "qiime tools export "
        "--input-path {input.Artifact} "
        "--output-path {output.Export}"

###############################################################################
rule export_subtables:
    # Aim: Export QIIME2 subtables artifact.qza
    # Use: qiime tools export [OPTIONS] PATH
    conda:
        QIIME2
    input:
        Artifact = "out/{project}/{primers}/qiime2/subtables/{subt}.qza"
    output:
        Export = directory("out/{project}/{primers}/qiime2/export/subtables/"
                           +"{subt}/")
    shell:
        "qiime tools export "
        "--input-path {input.Artifact} "
        "--output-path {output.Export}"

###############################################################################

