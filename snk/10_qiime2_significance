###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: statistically compare groups of alpha/beta diversity values
# Date: 2017.11.02
# Run: snakemake -j [core] --use-conda
# Latest modification: 2018.11.19
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]
PRIMERS = config["datasets"]["primers"]
ALPHA = config["diversity"]["alpha"]["significance"]
BETA = config["diversity"]["beta"]["significance"]
CATEGORIES = config["permanova"]["categories"]

# Environment
QIIME2 = "../env/qiime2-2018.08.yaml"

# Params
METADATA = config["datasets"]["metadata"] # general

CORRELATION = config["diversity"]["alpha"]["correlation"] # alpha_correlation

METHODE =  config["permanova"]["methode"]          # beta_diversity
PAIRWISE = config["permanova"]["pairwise"]         # beta_diversity
PERMUTATIONS = config["permanova"]["permutations"] # beta_diversity

################################################################################
rule all:
    input:
        alpha_significance = expand("out/{project}/{primers}/qiime2/visual/"
                                    +"AlphaSignification-{alpha}.qzv",
                                    project = PROJECT,
                                    primers = PRIMERS,
                                    alpha = ALPHA),

        alpha_correlation = expand("out/{project}/{primers}/qiime2/visual/"
                                   +"AlphaCorrelation-{alpha}.qzv",
                                   project = PROJECT,
                                   primers = PRIMERS,
                                   alpha = ALPHA),
        
        beta_significance = expand("out/{project}/{primers}/qiime2/visual/"
                                   +"BetaSignification-{beta}-{categories}.qzv",
                                   project = PROJECT,
                                   primers = PRIMERS,
                                   beta = BETA,
                                   categories = CATEGORIES)

################################################################################
rule beta_diversity_group_significance:
    # Aim: Determine whether groups of samples are significantly different
     # from one another using a permutation-based statistical test
    # Use: qiime diversity beta-group-significance [OPTIONS]
    conda:
        QIIME2
    params:
        metadata = METADATA,
        methode = METHODE,
        pairwise = PAIRWISE,
        permutations = PERMUTATIONS
    input:
        matrix = "out/{project}/{primers}/qiime2/core/Matrix-{beta}.qza"
    output:
        visualization = ("out/{project}/{primers}/qiime2/visual/"
                         +"BetaSignification-{beta}-{categories}.qzv")
    shell:
        "qiime diversity beta-group-significance "
        "--i-distance-matrix {input.matrix} "
        "--m-metadata-file {params.metadata} "
        "--m-metadata-column {wildcards.categories} "
        "--o-visualization {output.visualization} "
        "--p-method {params.methode} "
        "--p-{params.pairwise} "
        "--p-permutations {params.permutations}"

################################################################################
rule alpha_diversity_correlation:
    # Aim: Determine whether numeric sample metadata category is correlated
     # with alpha diversity
    # Use: qiime diversity alpha-correlation [OPTIONS]
    conda:
        QIIME2
    params:
        metadata = METADATA,
        correlation = CORRELATION
    input:
        vector = "out/{project}/{primers}/qiime2/core/Vector-{alpha}.qza"
    output:
        visualization = ("out/{project}/{primers}/qiime2/visual/"
                         +"AlphaCorrelation-{alpha}.qzv")
    shell:
        "qiime diversity alpha-correlation "
        "--i-alpha-diversity {input.vector} "
        "--m-metadata-file {params.metadata} "
        "--p-method {params.correlation} "
        "--o-visualization {output.visualization}"

################################################################################
rule alpha_diversity_group_significance:
    # Aim: Visually and statistically compare groups of alpha diversity values
    # Use: qiime diversity alpha-group-significance [OPTIONS]
    conda:
        QIIME2
    params:
        metadata = METADATA
    input:
        vector = "out/{project}/{primers}/qiime2/core/Vector-{alpha}.qza"
    output:
        visualization = ("out/{project}/{primers}/qiime2/visual/"
                         +"AlphaSignification-{alpha}.qzv")
    shell:
        "qiime diversity alpha-group-significance "
        "--i-alpha-diversity {input.vector} "
        "--m-metadata-file {params.metadata} "
        "--o-visualization {output.visualization}"

################################################################################
