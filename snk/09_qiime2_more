###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: compute user-specified diversity metrics and pcoa with emperor plot 
# Date: 2017.11.02
# Run: snakemake -j [core] --use-conda
# Latest modification: 2018.11.16
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]
PRIMERS = config["datasets"]["primers"]
ALPHA = config["diversity"]["alpha"]["more"]
BETA = config["diversity"]["beta"]["more"]

# Environment
QIIME2 = "../env/qiime2-2018.08.yaml"

# Params
JOBS = config["cluster"]["threads"] # beta

METADATA = config["datasets"]["metadata"] # emperor


################################################################################
rule all:
    input:
        vector = expand("out/{project}/{primers}/qiime2/core/"
                        +"Vector-{alpha}.qza",
                        project = PROJECT,
                        primers = PRIMERS,
                        alpha = ALPHA),
        
        emperor = expand("out/{project}/{primers}/qiime2/visual/"
                         +"Emperor-{beta}.qzv",
                         project = PROJECT,
                         primers = PRIMERS,
                         beta = BETA)

###############################################################################
rule emperor_plot:
    # Aim: Generate visualization of your ordination
    # Use: qiime emperor plot [OPTIONS]
    conda:
        QIIME2
    params:
        metadata = METADATA
    input:
        pcoa = "out/{project}/{primers}/qiime2/pcoa/PCoA-{beta}.qza"
    output:
        visualization = ("out/{project}/{primers}/qiime2/visual/"
                         +"Emperor-{beta}.qzv")
    shell:
        "qiime emperor plot "
        "--i-pcoa {input.pcoa} "
        "--m-metadata-file {params.metadata} "
        "--o-visualization {output.visualization}"

###############################################################################
rule beta_diversity_pcoa:
    # Aim: Apply Principal Coordinate Analysis
    # Use: qiime diversity pcoa [OPTIONS]
    conda:
        QIIME2
    input:
        matrix = "out/{project}/{primers}/qiime2/core/Matrix-{beta}.qza"
    output:
        pcoa = "out/{project}/{primers}/qiime2/pcoa/PCoA-{beta}.qza"
    shell:
        "qiime diversity pcoa "
        "--i-distance-matrix {input.matrix} "
        "--o-pcoa {output.pcoa}"

################################################################################
rule beta_diversity:
    # Aim: Compute a user-specified beta diversity metric,
     # for all pairs of samples in a feature table
    # Use: qiime diversity beta [OPTIONS]
    conda:
        QIIME2
    params:
        jobs = JOBS
    input:
        filtered_table = "out/{project}/{primers}/qiime2/core/RarTable.qza"
    output:
        matrix = "out/{project}/{primers}/qiime2/core/Matrix-{beta}.qza"
    shell:
        "qiime diversity beta "
        "--i-table {input.filtered_table} "
        "--p-metric {wildcards.beta} "
        "--p-n-jobs {params.jobs} "
        "--o-distance-matrix {output.matrix}"

################################################################################
rule alpha_diversity:
    # Aim: Compute a user-specified alpha diversity metric,
     # for all samples in a feature table
    # Use: qiime diversity alpha [OPTIONS]
    conda:
        QIIME2
    input:
        filtered_table = "out/{project}/{primers}/qiime2/core/RarTable.qza"
    output:
        vector = "out/{project}/{primers}/qiime2/core/Vector-{alpha}.qza"
    shell:
        "qiime diversity alpha "
        "--i-table {input.filtered_table} "
        "--p-metric {wildcards.alpha} "
        "--o-alpha-diversity {output.vector}"

################################################################################
